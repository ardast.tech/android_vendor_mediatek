/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2019. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 *     TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/

.HFG = {
    imx258mipimono_HFG_0000[0],imx258mipimono_HFG_0000[1],imx258mipimono_HFG_0000[2],imx258mipimono_HFG_0000[3],imx258mipimono_HFG_0000[4],imx258mipimono_HFG_0000[5],imx258mipimono_HFG_0000[6],imx258mipimono_HFG_0000[7],imx258mipimono_HFG_0008[0],imx258mipimono_HFG_0008[1],
    imx258mipimono_HFG_0008[2],imx258mipimono_HFG_0008[3],imx258mipimono_HFG_0008[4],imx258mipimono_HFG_0008[5],imx258mipimono_HFG_0008[6],imx258mipimono_HFG_0008[7],imx258mipimono_HFG_0016[0],imx258mipimono_HFG_0016[1],imx258mipimono_HFG_0016[2],imx258mipimono_HFG_0016[3],
    imx258mipimono_HFG_0016[4],imx258mipimono_HFG_0016[5],imx258mipimono_HFG_0016[6],imx258mipimono_HFG_0016[7],imx258mipimono_HFG_0024[0],imx258mipimono_HFG_0024[1],imx258mipimono_HFG_0024[2],imx258mipimono_HFG_0024[3],imx258mipimono_HFG_0024[4],imx258mipimono_HFG_0024[5],
    imx258mipimono_HFG_0024[6],imx258mipimono_HFG_0024[7],
},
.SWNR_THRES = {
    imx258mipimono_SWNR_THRES_0000[0],imx258mipimono_SWNR_THRES_0001[0],imx258mipimono_SWNR_THRES_0002[0],imx258mipimono_SWNR_THRES_0003[0],
},
.SWNR = {
    imx258mipimono_SWNR_0000[0],imx258mipimono_SWNR_0000[1],imx258mipimono_SWNR_0000[2],imx258mipimono_SWNR_0000[3],imx258mipimono_SWNR_0000[4],imx258mipimono_SWNR_0000[5],imx258mipimono_SWNR_0000[6],imx258mipimono_SWNR_0000[7],imx258mipimono_SWNR_0008[0],imx258mipimono_SWNR_0008[1],
    imx258mipimono_SWNR_0008[2],imx258mipimono_SWNR_0008[3],imx258mipimono_SWNR_0008[4],imx258mipimono_SWNR_0008[5],imx258mipimono_SWNR_0008[6],imx258mipimono_SWNR_0008[7],imx258mipimono_SWNR_0016[0],imx258mipimono_SWNR_0016[1],imx258mipimono_SWNR_0016[2],imx258mipimono_SWNR_0016[3],
    imx258mipimono_SWNR_0016[4],imx258mipimono_SWNR_0016[5],imx258mipimono_SWNR_0016[6],imx258mipimono_SWNR_0016[7],imx258mipimono_SWNR_0024[0],imx258mipimono_SWNR_0024[1],imx258mipimono_SWNR_0024[2],imx258mipimono_SWNR_0024[3],imx258mipimono_SWNR_0024[4],imx258mipimono_SWNR_0024[5],
    imx258mipimono_SWNR_0024[6],imx258mipimono_SWNR_0024[7],
},
.MFNR = {
    imx258mipimono_MFNR_0000[0],imx258mipimono_MFNR_0000[1],imx258mipimono_MFNR_0000[2],imx258mipimono_MFNR_0000[3],imx258mipimono_MFNR_0000[4],imx258mipimono_MFNR_0000[5],imx258mipimono_MFNR_0000[6],imx258mipimono_MFNR_0000[7],imx258mipimono_MFNR_0008[0],imx258mipimono_MFNR_0008[1],
    imx258mipimono_MFNR_0008[2],imx258mipimono_MFNR_0008[3],imx258mipimono_MFNR_0008[4],imx258mipimono_MFNR_0008[5],imx258mipimono_MFNR_0008[6],imx258mipimono_MFNR_0008[7],
},
.BSS = {
    imx258mipimono_BSS_0000[0],imx258mipimono_BSS_0000[1],imx258mipimono_BSS_0000[2],imx258mipimono_BSS_0000[3],imx258mipimono_BSS_0000[4],imx258mipimono_BSS_0000[5],imx258mipimono_BSS_0000[6],imx258mipimono_BSS_0000[7],imx258mipimono_BSS_0008[0],imx258mipimono_BSS_0008[1],
    imx258mipimono_BSS_0008[2],imx258mipimono_BSS_0008[3],imx258mipimono_BSS_0008[4],imx258mipimono_BSS_0008[5],imx258mipimono_BSS_0008[6],imx258mipimono_BSS_0008[7],
},
.CA_LTM = {
    imx258mipimono_CA_LTM_0000[0],imx258mipimono_CA_LTM_0001[0],imx258mipimono_CA_LTM_0002[0],imx258mipimono_CA_LTM_0003[0],imx258mipimono_CA_LTM_0004[0],imx258mipimono_CA_LTM_0005[0],imx258mipimono_CA_LTM_0006[0],imx258mipimono_CA_LTM_0007[0],imx258mipimono_CA_LTM_0008[0],imx258mipimono_CA_LTM_0009[0],
    imx258mipimono_CA_LTM_0010[0],imx258mipimono_CA_LTM_0011[0],imx258mipimono_CA_LTM_0012[0],imx258mipimono_CA_LTM_0013[0],imx258mipimono_CA_LTM_0014[0],
},
.ClearZoom = {
    imx258mipimono_ClearZoom_0000[0],imx258mipimono_ClearZoom_0001[0],
},
.AINR = {
    imx258mipimono_AINR_0000[0],imx258mipimono_AINR_0000[1],imx258mipimono_AINR_0000[2],imx258mipimono_AINR_0000[3],imx258mipimono_AINR_0000[4],imx258mipimono_AINR_0000[5],imx258mipimono_AINR_0000[6],imx258mipimono_AINR_0000[7],
},
.DSDN = {
    imx258mipimono_DSDN_0000[0],imx258mipimono_DSDN_0001[0],
},
.MFNR_THRES = {
    imx258mipimono_MFNR_THRES_0000[0],imx258mipimono_MFNR_THRES_0001[0],
},
.AINR_THRES = {
    imx258mipimono_AINR_THRES_0000[0],
},
