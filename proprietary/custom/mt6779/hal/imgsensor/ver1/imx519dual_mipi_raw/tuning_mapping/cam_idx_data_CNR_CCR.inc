#define IDX_DATA_CNR_CCR_DIM_NS    6
#define IDX_DATA_CNR_CCR_FACTOR_SZ    3
#define IDX_DATA_CNR_CCR_ENTRY_NS    26

static unsigned int _cam_data_entry_CNR_CCR_key0000[] = {0X000001FC, 0X80700000, 0X00016700, };
static unsigned int _cam_data_entry_CNR_CCR_key0001[] = {0X000001FC, 0X40700000, 0X00016700, };
static unsigned int _cam_data_entry_CNR_CCR_key0002[] = {0X000001FC, 0X80700000, 0X0001A700, };
static unsigned int _cam_data_entry_CNR_CCR_key0003[] = {0X000001FC, 0X40700000, 0X0001A700, };
static unsigned int _cam_data_entry_CNR_CCR_key0004[] = {0X00000001, 0X40040000, 0X000F7D02, };
static unsigned int _cam_data_entry_CNR_CCR_key0005[] = {0X00000001, 0X80040000, 0X00016500, };
static unsigned int _cam_data_entry_CNR_CCR_key0006[] = {0X00000001, 0X00040000, 0X000F7D01, };
static unsigned int _cam_data_entry_CNR_CCR_key0007[] = {0X00000001, 0X40040000, 0X000FBD02, };
static unsigned int _cam_data_entry_CNR_CCR_key0008[] = {0X00000001, 0X80040000, 0X0001A500, };
static unsigned int _cam_data_entry_CNR_CCR_key0009[] = {0X00000001, 0X00040000, 0X000FBD01, };
static unsigned int _cam_data_entry_CNR_CCR_key0010[] = {0X00000002, 0X40080000, 0X000FFD02, };
static unsigned int _cam_data_entry_CNR_CCR_key0011[] = {0X00000002, 0X00080000, 0X000FFD01, };
static unsigned int _cam_data_entry_CNR_CCR_key0012[] = {0X00CC0000, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_CNR_CCR_key0013[] = {0X03300000, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_CNR_CCR_key0014[] = {0XFC000000, 0XC000000F, 0X000FC7FF, };
static unsigned int _cam_data_entry_CNR_CCR_key0015[] = {0X00000600, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_CNR_CCR_key0016[] = {0X00001800, 0XC0000000, 0X000FFFFF, };
static unsigned int _cam_data_entry_CNR_CCR_key0017[] = {0X0001E000, 0XC0000000, 0X000FC7FF, };
static unsigned int _cam_data_entry_CNR_CCR_key0018[] = {0X0001E1FC, 0XC0700000, 0X000FDBFF, };
static unsigned int _cam_data_entry_CNR_CCR_key0019[] = {0X00000001, 0X80040000, 0X00015900, };
static unsigned int _cam_data_entry_CNR_CCR_key0020[] = {0X00000001, 0X80040000, 0X00019900, };
static unsigned int _cam_data_entry_CNR_CCR_key0021[] = {0X000001FC, 0X80700000, 0X0006FF00, };
static unsigned int _cam_data_entry_CNR_CCR_key0022[] = {0X00000001, 0X80040000, 0X0006FF00, };
static unsigned int _cam_data_entry_CNR_CCR_key0023[] = {0XFFFFFFFF, 0XBFFFFFFF, 0X000FFF00, };
static unsigned int _cam_data_entry_CNR_CCR_key0024[] = {0XFFFFFFFF, 0X7FFFFFFF, 0X000FFFF8, };
static unsigned int _cam_data_entry_CNR_CCR_key0025[] = {0XFFFFFFFF, 0X3FFFFFFF, 0X000FFF07, };

static IDX_MASK_ENTRY _cam_data_entry_CNR_CCR[IDX_DATA_CNR_CCR_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0000, 0, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0001, 0, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0002, 0, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0003, 0, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0004, 0, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0005, 0, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0006, 0, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0007, 0, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0008, 0, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0009, 0, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0010, 0, 10, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0011, 0, 11, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0012, 0, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0013, 0, 13, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0014, 0, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0015, 0, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0016, 0, 16, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0017, 0, 17, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0018, 0, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0019, 0, 19, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0020, 0, 20, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0021, 0, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0022, 0, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0023, 0, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0024, 0, 25, 0, 0},
    {(unsigned int*)&_cam_data_entry_CNR_CCR_key0025, 0, 26, 0, 0},
};

static unsigned short _cam_data_dims_CNR_CCR[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_FrontBin,
    EDim_Flash,
    EDim_FaceDetection,
    EDim_Zoom,
};

static unsigned short _cam_data_expand_CNR_CCR[] = 
{0, 0, 1};

const IDX_MASK_T cam_data_CNR_CCR =
{
    {IDX_ALGO_MASK, IDX_DATA_CNR_CCR_DIM_NS, (unsigned short*)&_cam_data_dims_CNR_CCR, (unsigned short*)&_cam_data_expand_CNR_CCR},
    {IDX_DATA_CNR_CCR_ENTRY_NS, IDX_DATA_CNR_CCR_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_CNR_CCR}
};