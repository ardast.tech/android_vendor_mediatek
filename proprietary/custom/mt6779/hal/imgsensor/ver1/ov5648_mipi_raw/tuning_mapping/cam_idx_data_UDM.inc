#define IDX_DATA_UDM_DIM_NS    5
#define IDX_DATA_UDM_FACTOR_SZ    4
#define IDX_DATA_UDM_ENTRY_NS    56

static unsigned int _cam_data_entry_UDM_key0000[] = {0X00000004, 0X00000000, 0X00800000, 0X0000060F, };
static unsigned int _cam_data_entry_UDM_key0001[] = {0X00000004, 0X00000000, 0X00800000, 0X0000002F, };
static unsigned int _cam_data_entry_UDM_key0002[] = {0X00000004, 0X00000000, 0X00800000, 0X000007EF, };
static unsigned int _cam_data_entry_UDM_key0003[] = {0X00000008, 0X00000000, 0X00800000, 0X0000060F, };
static unsigned int _cam_data_entry_UDM_key0004[] = {0X00000008, 0X00000000, 0X00800000, 0X0000002F, };
static unsigned int _cam_data_entry_UDM_key0005[] = {0X00000008, 0X00000000, 0X00800000, 0X000007EF, };
static unsigned int _cam_data_entry_UDM_key0006[] = {0X00000004, 0X00000000, 0X00400000, 0X0000060F, };
static unsigned int _cam_data_entry_UDM_key0007[] = {0X00000004, 0X00000000, 0X00400000, 0X0000002F, };
static unsigned int _cam_data_entry_UDM_key0008[] = {0X00000004, 0X00000000, 0X00400000, 0X000007EF, };
static unsigned int _cam_data_entry_UDM_key0009[] = {0X00000008, 0X00000000, 0X00400000, 0X0000060F, };
static unsigned int _cam_data_entry_UDM_key0010[] = {0X00000008, 0X00000000, 0X00400000, 0X0000002F, };
static unsigned int _cam_data_entry_UDM_key0011[] = {0X00000008, 0X00000000, 0X00400000, 0X000007EF, };
static unsigned int _cam_data_entry_UDM_key0012[] = {0X00000004, 0X00000000, 0X00800000, 0X000007F7, };
static unsigned int _cam_data_entry_UDM_key0013[] = {0X00000008, 0X00000000, 0X00800000, 0X000007F7, };
static unsigned int _cam_data_entry_UDM_key0014[] = {0X00000004, 0X00000000, 0X00400000, 0X000007F7, };
static unsigned int _cam_data_entry_UDM_key0015[] = {0X00000008, 0X00000000, 0X00400000, 0X000007F7, };
static unsigned int _cam_data_entry_UDM_key0016[] = {0X00000001, 0X00000000, 0X00400040, 0X000007EF, };
static unsigned int _cam_data_entry_UDM_key0017[] = {0X00000001, 0X00000000, 0X00800040, 0X000007E9, };
static unsigned int _cam_data_entry_UDM_key0018[] = {0X00000001, 0X00000000, 0X01000040, 0X000007EF, };
static unsigned int _cam_data_entry_UDM_key0019[] = {0X00000001, 0X00000000, 0X00400040, 0X000007F7, };
static unsigned int _cam_data_entry_UDM_key0020[] = {0X00000001, 0X00000000, 0X00800040, 0X000007F1, };
static unsigned int _cam_data_entry_UDM_key0021[] = {0X00000001, 0X00000000, 0X01000040, 0X000007F7, };
static unsigned int _cam_data_entry_UDM_key0022[] = {0X00000002, 0X00000000, 0X00400080, 0X000007FF, };
static unsigned int _cam_data_entry_UDM_key0023[] = {0X00000002, 0X00000000, 0X01000080, 0X000007FF, };
static unsigned int _cam_data_entry_UDM_key0024[] = {0X00000000, 0X0000071C, 0XFFC1C000, 0X000007FF, };
static unsigned int _cam_data_entry_UDM_key0025[] = {0X00000000, 0X000038E0, 0XFFCE0000, 0X000007FF, };
static unsigned int _cam_data_entry_UDM_key0026[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_UDM_key0027[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_UDM_key0028[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_UDM_key0029[] = {0X01C70000, 0X00000000, 0XFFC00700, 0X000007FF, };
static unsigned int _cam_data_entry_UDM_key0030[] = {0X0E380000, 0X00000000, 0XFFC03800, 0X000007FF, };
static unsigned int _cam_data_entry_UDM_key0031[] = {0X30000000, 0X00000000, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_UDM_key0032[] = {0X30000000, 0X00000000, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_UDM_key0033[] = {0X30000000, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_UDM_key0034[] = {0X40000000, 0X00000000, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_UDM_key0035[] = {0X40000000, 0X00000000, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_UDM_key0036[] = {0X40000000, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_UDM_key0037[] = {0X00000300, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_UDM_key0038[] = {0X00000C00, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_UDM_key0039[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X00000619, };
static unsigned int _cam_data_entry_UDM_key0040[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X00000039, };
static unsigned int _cam_data_entry_UDM_key0041[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X000007F9, };
static unsigned int _cam_data_entry_UDM_key0042[] = {0X0000F004, 0X00000000, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_UDM_key0043[] = {0X0000F004, 0X00000000, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_UDM_key0044[] = {0X0000F004, 0X00000000, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_UDM_key0045[] = {0X00000008, 0X00000000, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_UDM_key0046[] = {0X00000008, 0X00000000, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_UDM_key0047[] = {0X00000008, 0X00000000, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_UDM_key0048[] = {0X30000000, 0X0000C000, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_UDM_key0049[] = {0X30000000, 0X0000C000, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_UDM_key0050[] = {0X30000000, 0X0000C000, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_UDM_key0051[] = {0X40000000, 0X00000000, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_UDM_key0052[] = {0X40000000, 0X00000000, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_UDM_key0053[] = {0X40000000, 0X00000000, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_UDM_key0054[] = {0X00000001, 0X00000000, 0X00800040, 0X000007EA, };
static unsigned int _cam_data_entry_UDM_key0055[] = {0X00000001, 0X00000000, 0X00800040, 0X000007F2, };

static IDX_MASK_ENTRY _cam_data_entry_UDM[IDX_DATA_UDM_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_UDM_key0000, 0, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0001, 11, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0002, 1, 0, 6, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0003, 12, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0004, 23, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0005, 13, 0, 6, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0006, 0, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0007, 11, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0008, 1, 1, 6, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0009, 12, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0010, 23, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0011, 13, 1, 6, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0012, 24, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0013, 32, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0014, 24, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0015, 32, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0016, 40, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0017, 40, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0018, 40, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0019, 40, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0020, 40, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0021, 40, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0022, 48, 10, 2, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0023, 48, 11, 2, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0024, 40, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0025, 48, 13, 2, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0026, 0, 14, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0027, 11, 14, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0028, 1, 14, 6, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0029, 40, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0030, 48, 16, 2, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0031, 0, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0032, 11, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0033, 1, 17, 6, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0034, 12, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0035, 23, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0036, 13, 17, 6, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0037, 40, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0038, 48, 19, 2, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0039, 0, 20, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0040, 11, 20, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0041, 1, 20, 6, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0042, 0, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0043, 11, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0044, 1, 21, 6, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0045, 12, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0046, 23, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0047, 13, 21, 6, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0048, 0, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0049, 11, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0050, 1, 22, 6, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0051, 12, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0052, 23, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_UDM_key0053, 13, 22, 6, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0054, 40, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_UDM_key0055, 40, 24, 0, 0},
};

static unsigned short _cam_data_dims_UDM[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_Flash,
    EDim_FaceDetection,
    EDim_LV,
};

static unsigned short _cam_data_expand_UDM[] = 
{0, 0, 1};

const IDX_MASK_T cam_data_UDM =
{
    {IDX_ALGO_MASK, IDX_DATA_UDM_DIM_NS, (unsigned short*)&_cam_data_dims_UDM, (unsigned short*)&_cam_data_expand_UDM},
    {IDX_DATA_UDM_ENTRY_NS, IDX_DATA_UDM_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_UDM}
};