#define IDX_DATA_NBC2_CCR_DIM_NS    5
#define IDX_DATA_NBC2_CCR_FACTOR_SZ    4
#define IDX_DATA_NBC2_CCR_ENTRY_NS    75

static unsigned int _cam_data_entry_NBC2_CCR_key0000[] = {0X0000007C, 0X00000000, 0X00800000, 0X0000060F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0001[] = {0X0000007C, 0X00000000, 0X00800000, 0X0000002F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0002[] = {0X0000007C, 0X00000000, 0X00800000, 0X000007EF, };
static unsigned int _cam_data_entry_NBC2_CCR_key0003[] = {0X0000007C, 0X00000000, 0X00400000, 0X0000060F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0004[] = {0X0000007C, 0X00000000, 0X00400000, 0X0000002F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0005[] = {0X0000007C, 0X00000000, 0X00400000, 0X000007EF, };
static unsigned int _cam_data_entry_NBC2_CCR_key0006[] = {0X0000007C, 0X00000000, 0X00800000, 0X00000617, };
static unsigned int _cam_data_entry_NBC2_CCR_key0007[] = {0X0000007C, 0X00000000, 0X00800000, 0X00000037, };
static unsigned int _cam_data_entry_NBC2_CCR_key0008[] = {0X0000007C, 0X00000000, 0X00800000, 0X000007F7, };
static unsigned int _cam_data_entry_NBC2_CCR_key0009[] = {0X0000007C, 0X00000000, 0X00400000, 0X00000617, };
static unsigned int _cam_data_entry_NBC2_CCR_key0010[] = {0X0000007C, 0X00000000, 0X00400000, 0X00000037, };
static unsigned int _cam_data_entry_NBC2_CCR_key0011[] = {0X0000007C, 0X00000000, 0X00400000, 0X000007F7, };
static unsigned int _cam_data_entry_NBC2_CCR_key0012[] = {0X00000001, 0X00000000, 0X00400040, 0X0000060F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0013[] = {0X00000001, 0X00000000, 0X00400040, 0X0000002F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0014[] = {0X00000001, 0X00000000, 0X00400040, 0X000007EF, };
static unsigned int _cam_data_entry_NBC2_CCR_key0015[] = {0X00000001, 0X00000000, 0X00800040, 0X00000609, };
static unsigned int _cam_data_entry_NBC2_CCR_key0016[] = {0X00000001, 0X00000000, 0X00800040, 0X00000029, };
static unsigned int _cam_data_entry_NBC2_CCR_key0017[] = {0X00000001, 0X00000000, 0X00800040, 0X000007E9, };
static unsigned int _cam_data_entry_NBC2_CCR_key0018[] = {0X00000001, 0X00000000, 0X01000040, 0X0000060F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0019[] = {0X00000001, 0X00000000, 0X01000040, 0X0000002F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0020[] = {0X00000001, 0X00000000, 0X01000040, 0X000007EF, };
static unsigned int _cam_data_entry_NBC2_CCR_key0021[] = {0X00000001, 0X00000000, 0X00400040, 0X00000617, };
static unsigned int _cam_data_entry_NBC2_CCR_key0022[] = {0X00000001, 0X00000000, 0X00400040, 0X00000037, };
static unsigned int _cam_data_entry_NBC2_CCR_key0023[] = {0X00000001, 0X00000000, 0X00400040, 0X000007F7, };
static unsigned int _cam_data_entry_NBC2_CCR_key0024[] = {0X00000001, 0X00000000, 0X00800040, 0X00000611, };
static unsigned int _cam_data_entry_NBC2_CCR_key0025[] = {0X00000001, 0X00000000, 0X00800040, 0X00000031, };
static unsigned int _cam_data_entry_NBC2_CCR_key0026[] = {0X00000001, 0X00000000, 0X00800040, 0X000007F1, };
static unsigned int _cam_data_entry_NBC2_CCR_key0027[] = {0X00000001, 0X00000000, 0X01000040, 0X00000617, };
static unsigned int _cam_data_entry_NBC2_CCR_key0028[] = {0X00000001, 0X00000000, 0X01000040, 0X00000037, };
static unsigned int _cam_data_entry_NBC2_CCR_key0029[] = {0X00000001, 0X00000000, 0X01000040, 0X000007F7, };
static unsigned int _cam_data_entry_NBC2_CCR_key0030[] = {0X00000002, 0X00000000, 0X00400080, 0X0000061F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0031[] = {0X00000002, 0X00000000, 0X00400080, 0X0000003F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0032[] = {0X00000002, 0X00000000, 0X00400080, 0X000007FF, };
static unsigned int _cam_data_entry_NBC2_CCR_key0033[] = {0X00000002, 0X00000000, 0X01000080, 0X0000061F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0034[] = {0X00000002, 0X00000000, 0X01000080, 0X0000003F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0035[] = {0X00000002, 0X00000000, 0X01000080, 0X000007FF, };
static unsigned int _cam_data_entry_NBC2_CCR_key0036[] = {0X00000000, 0X0000071C, 0XFFC1C000, 0X0000061F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0037[] = {0X00000000, 0X0000071C, 0XFFC1C000, 0X0000003F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0038[] = {0X00000000, 0X0000071C, 0XFFC1C000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC2_CCR_key0039[] = {0X00000000, 0X000038E0, 0XFFCE0000, 0X0000061F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0040[] = {0X00000000, 0X000038E0, 0XFFCE0000, 0X0000003F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0041[] = {0X00000000, 0X000038E0, 0XFFCE0000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC2_CCR_key0042[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0043[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0044[] = {0X00000000, 0X0000C000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC2_CCR_key0045[] = {0X01C70000, 0X00000000, 0XFFC00700, 0X0000061F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0046[] = {0X01C70000, 0X00000000, 0XFFC00700, 0X0000003F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0047[] = {0X01C70000, 0X00000000, 0XFFC00700, 0X000007FF, };
static unsigned int _cam_data_entry_NBC2_CCR_key0048[] = {0X0E380000, 0X00000000, 0XFFC03800, 0X0000061F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0049[] = {0X0E380000, 0X00000000, 0XFFC03800, 0X0000003F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0050[] = {0X0E380000, 0X00000000, 0XFFC03800, 0X000007FF, };
static unsigned int _cam_data_entry_NBC2_CCR_key0051[] = {0XF0000000, 0X00000003, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0052[] = {0XF0000000, 0X00000003, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0053[] = {0XF0000000, 0X00000003, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC2_CCR_key0054[] = {0X00000300, 0X00000000, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0055[] = {0X00000300, 0X00000000, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0056[] = {0X00000300, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC2_CCR_key0057[] = {0X00000C00, 0X00000000, 0XFFC00000, 0X0000061F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0058[] = {0X00000C00, 0X00000000, 0XFFC00000, 0X0000003F, };
static unsigned int _cam_data_entry_NBC2_CCR_key0059[] = {0X00000C00, 0X00000000, 0XFFC00000, 0X000007FF, };
static unsigned int _cam_data_entry_NBC2_CCR_key0060[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X00000619, };
static unsigned int _cam_data_entry_NBC2_CCR_key0061[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X00000039, };
static unsigned int _cam_data_entry_NBC2_CCR_key0062[] = {0X0000F000, 0X00000000, 0XFFC00000, 0X000007F9, };
static unsigned int _cam_data_entry_NBC2_CCR_key0063[] = {0X0000F07C, 0X00000000, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_NBC2_CCR_key0064[] = {0X0000F07C, 0X00000000, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_NBC2_CCR_key0065[] = {0X0000F07C, 0X00000000, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_NBC2_CCR_key0066[] = {0XF0000000, 0X0000C003, 0XFFC00000, 0X0000061A, };
static unsigned int _cam_data_entry_NBC2_CCR_key0067[] = {0XF0000000, 0X0000C003, 0XFFC00000, 0X0000003A, };
static unsigned int _cam_data_entry_NBC2_CCR_key0068[] = {0XF0000000, 0X0000C003, 0XFFC00000, 0X000007FA, };
static unsigned int _cam_data_entry_NBC2_CCR_key0069[] = {0X00000001, 0X00000000, 0X00800040, 0X0000060A, };
static unsigned int _cam_data_entry_NBC2_CCR_key0070[] = {0X00000001, 0X00000000, 0X00800040, 0X0000002A, };
static unsigned int _cam_data_entry_NBC2_CCR_key0071[] = {0X00000001, 0X00000000, 0X00800040, 0X000007EA, };
static unsigned int _cam_data_entry_NBC2_CCR_key0072[] = {0X00000001, 0X00000000, 0X00800040, 0X00000612, };
static unsigned int _cam_data_entry_NBC2_CCR_key0073[] = {0X00000001, 0X00000000, 0X00800040, 0X00000032, };
static unsigned int _cam_data_entry_NBC2_CCR_key0074[] = {0X00000001, 0X00000000, 0X00800040, 0X000007F2, };

static IDX_MASK_ENTRY _cam_data_entry_NBC2_CCR[IDX_DATA_NBC2_CCR_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0000, 0, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0001, 11, 0, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0002, 1, 0, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0003, 0, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0004, 11, 1, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0005, 1, 1, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0006, 0, 2, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0007, 11, 2, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0008, 1, 2, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0009, 0, 3, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0010, 11, 3, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0011, 1, 3, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0012, 0, 4, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0013, 11, 4, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0014, 1, 4, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0015, 0, 5, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0016, 11, 5, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0017, 1, 5, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0018, 0, 6, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0019, 11, 6, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0020, 1, 6, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0021, 0, 7, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0022, 11, 7, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0023, 1, 7, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0024, 0, 8, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0025, 11, 8, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0026, 1, 8, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0027, 0, 9, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0028, 11, 9, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0029, 1, 9, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0030, 0, 10, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0031, 11, 10, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0032, 1, 10, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0033, 0, 11, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0034, 11, 11, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0035, 1, 11, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0036, 0, 12, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0037, 11, 12, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0038, 1, 12, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0039, 0, 13, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0040, 11, 13, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0041, 1, 13, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0042, 0, 14, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0043, 11, 14, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0044, 1, 14, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0045, 0, 15, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0046, 11, 15, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0047, 1, 15, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0048, 0, 16, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0049, 11, 16, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0050, 1, 16, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0051, 0, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0052, 11, 17, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0053, 1, 17, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0054, 0, 18, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0055, 11, 18, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0056, 1, 18, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0057, 0, 19, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0058, 11, 19, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0059, 1, 19, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0060, 0, 20, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0061, 11, 20, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0062, 1, 20, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0063, 0, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0064, 11, 21, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0065, 1, 21, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0066, 0, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0067, 11, 22, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0068, 1, 22, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0069, 0, 23, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0070, 11, 23, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0071, 1, 23, 6, 0},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0072, 0, 24, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0073, 11, 24, 6, 1},
    {(unsigned int*)&_cam_data_entry_NBC2_CCR_key0074, 1, 24, 6, 0},
};

static unsigned short _cam_data_dims_NBC2_CCR[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_Flash,
    EDim_FaceDetection,
    EDim_LV,
};

static unsigned short _cam_data_expand_NBC2_CCR[] = 
{0, 0, 1};

const IDX_MASK_T cam_data_NBC2_CCR =
{
    {IDX_ALGO_MASK, IDX_DATA_NBC2_CCR_DIM_NS, (unsigned short*)&_cam_data_dims_NBC2_CCR, (unsigned short*)&_cam_data_expand_NBC2_CCR},
    {IDX_DATA_NBC2_CCR_ENTRY_NS, IDX_DATA_NBC2_CCR_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_NBC2_CCR}
};